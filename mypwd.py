#!/usr/bin/python3

#    Copyright (c) 2019, IceGuye.

#    This program is distributed under the same BSD License.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the New
#    BSD License for more details.

#    You should have received a copy of the New BSD License along with
#    this program.  If not, see
#    <https://opensource.org/licenses/BSD-3-Clause>.

import os
import sys
import base64
import hashlib

arg_ls = sys.argv
if (len(arg_ls) != 2 and len(arg_ls) != 3):
    error = '''
You must enter the website's name.

Usage:

./mypwd example.com
    '''
    print(error)
    sys.exit()
elif len(arg_ls) == 2:
    name = str(arg_ls[1])
    length = 32
elif len(arg_ls) == 3:
    name = str(arg_ls[1])
    length = int(arg_ls[2])

check_salt = os.path.isfile('./mysalt.txt')

if check_salt == True:
    text_file = open("./mysalt.txt", "r")
else:
    text_file = open("./mysalt.txt", "w")
    print('You have no salt file. Generating... please be patient...')
    myrand = open("/dev/random", 'rb')
    myrand_4096 = myrand.read(4096)
    myrand_base64 = base64.b64encode(myrand_4096)
    myrand_str = myrand_base64.decode()
    myrand_str = myrand_str.replace("+", "-")
    myrand_str = myrand_str.replace("/", "_")
    text_file.write(myrand_str)
    text_file.close()

text_file = open("./mysalt.txt", "r")
data = text_file.read()
data = name + data
data = data.encode()
data_sha512 = hashlib.sha512()
data_sha512.update(data)
data = data_sha512.digest()
result = base64.b64encode(data).decode()
result = result.replace("+", "-")
result = result.replace("/", "_")
result = result[:length]
print("You password for this site is:")
print(result)
text_file.close()
