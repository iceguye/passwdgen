/*
    Copyright (c) 2019 - 2020, IceGuye.

    This program is distributed under the same BSD License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the New
    BSD License for more details.

    You should have received a copy of the New BSD License along with
    this program.  If not, see
    <https://opensource.org/licenses/BSD-3-Clause>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/sha.h>
#include <openssl/pem.h>

char *base64encode (const void *b64_encode_this, int encode_this_many_bytes){
    BIO *b64_bio, *mem_bio;      //Declares two OpenSSL BIOs: a base64 filter and a memory BIO.
    BUF_MEM *mem_bio_mem_ptr;    //Pointer to a "memory BIO" structure holding our base64 data.
    b64_bio = BIO_new(BIO_f_base64());                      //Initialize our base64 filter BIO.
    mem_bio = BIO_new(BIO_s_mem());                           //Initialize our memory sink BIO.
    BIO_push(b64_bio, mem_bio);            //Link the BIOs by creating a filter-sink BIO chain.
    BIO_set_flags(b64_bio, BIO_FLAGS_BASE64_NO_NL);  //No newlines every 64 characters or less.
    BIO_write(b64_bio, b64_encode_this, encode_this_many_bytes); //Records base64 encoded data.
    BIO_flush(b64_bio);   //Flush data.  Necessary for b64 encoding, because of pad characters.
    BIO_get_mem_ptr(mem_bio, &mem_bio_mem_ptr);  //Store address of mem_bio's memory structure.
    BIO_set_close(mem_bio, BIO_NOCLOSE);   //Permit access to mem_ptr after BIOs are destroyed.
    BIO_free_all(b64_bio);  //Destroys all BIOs in chain, starting with b64 (i.e. the 1st one).
    BUF_MEM_grow(mem_bio_mem_ptr, (*mem_bio_mem_ptr).length + 1);   //Makes space for end null.
    (*mem_bio_mem_ptr).data[(*mem_bio_mem_ptr).length] = '\0';  //Adds null-terminator to tail.
    return (*mem_bio_mem_ptr).data; //Returns base-64 encoded data. (See: "buf_mem_st" struct).
}

char *base64decode (const void *b64_decode_this, int decode_this_many_bytes){
    BIO *b64_bio, *mem_bio;      //Declares two OpenSSL BIOs: a base64 filter and a memory BIO.
    char *base64_decoded = calloc( (decode_this_many_bytes*3)/4+1, sizeof(char) ); //+1 = null.
    b64_bio = BIO_new(BIO_f_base64());                      //Initialize our base64 filter BIO.
    mem_bio = BIO_new(BIO_s_mem());                         //Initialize our memory source BIO.
    BIO_write(mem_bio, b64_decode_this, decode_this_many_bytes); //Base64 data saved in source.
    BIO_push(b64_bio, mem_bio);          //Link the BIOs by creating a filter-source BIO chain.
    BIO_set_flags(b64_bio, BIO_FLAGS_BASE64_NO_NL);          //Don't require trailing newlines.
    int decoded_byte_index = 0;   //Index where the next base64_decoded byte should be written.
    while ( 0 < BIO_read(b64_bio, base64_decoded+decoded_byte_index, 1) ){ //Read byte-by-byte.
        decoded_byte_index++; //Increment the index until read of BIO decoded data is complete.
    } //Once we're done reading decoded data, BIO_read returns -1 even though there's no error.
    BIO_free_all(b64_bio);  //Destroys all BIOs in chain, starting with b64 (i.e. the 1st one).
    return base64_decoded;        //Returns base-64 decoded data with trailing null terminator.
}

int main(int argc, char *argv[]) {
  size_t i;
  size_t file_size;
  size_t psw_len;
  long byte_count = 4096;
  char *website;
  if( argc == 2 ) {
    website = argv[1];
    psw_len = 32;
  }
  else if( argc == 3 ) {
    website = argv[1];
    psw_len = (int) *argv[2];
  }
  else {
    printf("Error: You must enter the website's name.\n");
    return 0;
  }
  FILE *fsalt;
  fsalt = fopen("./mysalt.txt", "r");
  if (fsalt == NULL) {
    printf("You don't have a salt file. We are generating one for you.\nplease be patient. It may take minutes.\n");
    FILE *frandev;
    frandev = fopen("/dev/random", "r");
    char tmp_data;
    char rand_data[byte_count];
    for (i = 0; i < byte_count; i = i + 1) {
      fread(&tmp_data, 1, 1, frandev);
      rand_data[i] = tmp_data;
    }
    fclose(frandev);
    char *mysalt;
    mysalt = base64encode(rand_data, sizeof(rand_data));
    fsalt = fopen("./mysalt.txt", "w");
    fprintf(fsalt, "%s\n", mysalt);
    fclose(fsalt);
    fsalt = fopen("./mysalt.txt", "r");
  }
  fseek (fsalt, 0, SEEK_END);
  file_size = ftell(fsalt);
  fseek (fsalt, 0, SEEK_SET);
  char salt_str[file_size];
  fread(&salt_str, 1, sizeof(salt_str), fsalt);
  char cat_data[(strlen(website) + strlen(salt_str)) * 10];
  strcat(cat_data, website);
  strcat(cat_data, salt_str);
  unsigned char hash[SHA512_DIGEST_LENGTH];
  SHA512(cat_data, strlen(cat_data), hash);
  char *myb64;
  myb64 = base64encode(hash, sizeof(hash));
  size_t myb64_len = strlen(myb64);
  for (i=0; i<myb64_len; i=i+1) {
    if (myb64[i] == '+') {
      myb64[i] = '-';
    } else if (myb64[i] == '/') {
      myb64[i] = '_';
    }
  }
  char final_result[psw_len+1];
  for (i = 0; i <= psw_len; i = i + 1) {
    final_result[i] = myb64[i];
    if (i == psw_len) {
      final_result[i] = '\0';
    }
  }
  
  printf("Your password is:\n%s\n",  final_result);
  fclose(fsalt);
  
  return 0;
}
